from twisted.internet import protocol, reactor, endpoints
from twisted.protocols import basic
import RPi.GPIO as GPIO
import time



### gpio query ################################################################

INPUT_PIN = 4
last_time = time.time()
delta_time = 0.0

GPIO.setmode(GPIO.BCM)
GPIO.setup(INPUT_PIN, GPIO.IN)

def on_contact_detected(chanel):
    global last_time
    global delta_time

    current_time = time.time();
    delta_time = current_time - last_time
    last_time = current_time




### twisted sensor server #####################################################

class sensor_protocol(protocol.Protocol):

    def connectionMade(self):
        self.transport.setTcpNoDelay(True)

    def dataReceived(self, data):
        global delta_time
        local_delta_time = time.time() - last_time

        if local_delta_time > delta_time:
            self.transport.write(str(local_delta_time).encode("utf-8"))
        else:
            self.transport.write(str(delta_time).encode("utf-8"))
        
        self.transport.loseConnection()

class sensor_factory(protocol.ServerFactory):
    protocol = sensor_protocol




### main ######################################################################

### GPIO EVENT HANDLER ###
GPIO.add_event_detect(INPUT_PIN, GPIO.FALLING, callback=on_contact_detected, bouncetime=25)

### wisted server ###
print("\nStarting server ...")
sensor_endpoint = endpoints.serverFromString(reactor, "tcp:8082")
sensor_endpoint.listen(sensor_factory())
reactor.run()