import RPi.GPIO as GPIO
import time
from http.server import BaseHTTPRequestHandler, HTTPServer

INPUT_PIN = 4

GPIO.setmode(GPIO.BCM)
GPIO.setup(INPUT_PIN,GPIO.IN)

last_time = time.time()
delta_time = 0.0

### setup #####################################################################


### classes ###################################################################

class http_server_request_handler(BaseHTTPRequestHandler):

	def create_json_message(self, delta):
		message = '{ "action" : "get_upm" , "payload" : { "upm" : ' + str(round(delta*1000)) + ' } }'
		return message
	
	def create_text_message(self, delta):
		message = 'upm:{0}'.format(round(delta*1000))
		return message

	def do_GET(self):
		global delta_time
		self.send_header('Content-Type', 'application/json')
		self.end_headers
		
		local_delta_time = time.time() - last_time
		if(local_delta_time > delta_time):
			message = self.create_text_message(local_delta_time)
		else:
			message = self.create_text_message(delta_time)
		
		
		self.wfile.write(bytes(message, "utf8"))
		return
		
### functions #################################################################

def run():
    print("Starting server...")

    # Server settings
    # Choose ort 8080, for port 80, which is normall used for a http server, you need root access
    server_address = ('192.168.2.115', 8082)
    httpd = HTTPServer(server_address, http_server_request_handler)
    print('running server...')
    httpd.serve_forever()
	
def on_contact_detected(channel):
	global last_time
	global delta_time
		
	current_time = time.time()
	delta_time = current_time - last_time
	last_time = current_time
	
	#print("contact detected on channel {} with delta_time: {}".format(channel, delta_time))

	
### main ######################################################################
	
GPIO.add_event_detect(INPUT_PIN, GPIO.FALLING, callback=on_contact_detected, bouncetime=25)
	
run()