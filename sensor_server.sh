#!/bin/sh
# launcher.sh
# navigate to home directory, then to this directory, then execute python script, then back home

cd /
cd home/pi/python_apps/05_sensor_http_server
sudo python3 sensor_http_server.py
cd /